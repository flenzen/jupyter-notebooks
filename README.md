# Notebooks
## SAT-Probleme
This notebook (in German) shows how to reduce the Hamilton circle problem and solving a Sudoku to an instance of SAT.
This is then passed to a SAT solver, from which a solution for the original problem is retrieved.

## Haskell-Schnelleinstieg
This Notebook (in German) is a quick introduction to the Haskell functional programming language.
It requires an IHaskell kernel.
I recommend using the IHaskell installation available on [mybinder](https://mybinder.org/v2/gh/gibiansky/IHaskell/mybinder).
